# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - test
  - build
  - deploy

include:
 - project: eclipse/oniro-core/oniro
   ref: kirkstone
   file:
    - .oniro-ci/dco.yaml
    - .oniro-ci/reuse.yaml
    - .oniro-ci/build-generic.yaml

dco:
  extends: .dco
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

reuse:
  extends: .reuse
  allow_failure: true
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

# Customize the .workspace job to set the path of the git repository to deviate
# from what the git-repo manifest prepares. This effectively allows testing
# incoming changes that match the repository holding this CI pipeline.
.workspace:
  variables:
    CI_ONIRO_GIT_REPO_PATH: docs

# Disable all the bitbake jobs, since we are not building any code here.
.bitbake-workspace:
  rules:
    - when: never

# Define a build-docs job that extends both the .workspace, for the general
# workspace setup, and .build-docs, for the documentation build logic. The
# script first assembles the workspace and then proceeds to build the
# documentation.
#
# The job extends more than one parent, with the order being relevant for,
# among others, the "rules" section.
build-docs:
  extends: [.workspace, .build-docs]
  variables:
    CI_ONIRO_INSTANCE_SIZE: s3.large.2
  script:
    - !reference [.workspace, script]
    - !reference [.build-docs, script]
    # Artifacts are relative to CI_PROJECT_DIR so we need to provide the build
    # docs there.
    - mv "$SCRATCH_DIR"/docs/build/ "$CI_PROJECT_DIR" || true
  artifacts:
    paths:
      - build
  rules:
    # Build the docs when a merge request is created.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    # Or when things land.
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

deploy:
  extends: .workspace
  stage: deploy
  script:
    - !reference [.workspace, script]
    # We are in the root of the git-repo workspace.
    - git clone https://user:$CI_ONIRO_AGGREGATED_DOCS_TOKEN@gitlab.eclipse.org/eclipse/oniro-core/oniro-readthedocs-aggregated.git aggregated
    - find aggregated -maxdepth 1 -not -path aggregated/.git -not -path aggregated -exec rm -rvf {} \;
    - tar -c --dereference -C docs --exclude ./.git --exclude ./.gitlab-ci.yml . | tar -x -C aggregated
    # Commit and push back, if something changed.
    - |
      cd aggregated;
      if [ -n "$(git status -s)" ]; then
        msg="docs repository snapshot - $CI_COMMIT_BRANCH:$CI_COMMIT_SHA";
        git add -A;
        git commit -sm "$msg";
        git push origin HEAD:main;
      else
        echo "Nothing new to commit.";
      fi
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
